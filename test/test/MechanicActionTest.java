package test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import action.MechanicAction;
import model.Mechanic;

public class MechanicActionTest {

	@Test
	public void addNewMechanic() {
		MechanicAction mechanicAction = new MechanicAction();
		Mechanic mechanic = new Mechanic("11","Ryan", "Luke", "65345678", "ryan.luck@gmail.com", "Blk 123, Robinson road, Singapore", "Master");
		List<Mechanic> mechanicList = mechanicAction.addNewMechanic(mechanic);
		assertEquals(mechanicList.size(), 11);
	}
	
	@Test
	public void updateMechanic() {
		MechanicAction mechanicAction = new MechanicAction();
		Mechanic mechanic = new Mechanic("1","Ryan", "Luke", "65345678", "ryan.luck@gmail.com", "Blk 123, Robinson road, Singapore", "Master");
		List<Mechanic> mechanicList = mechanicAction.updateMechanic(mechanic);
		assertEquals(mechanicList.get(0).getFirstName(), "Ryan");
	}
	
	@Test
	public void deleteMechanic() {
		MechanicAction mechanicAction = new MechanicAction();
		List<Mechanic> mechanicList = mechanicAction.deleteMechanic("1");
		assertEquals(mechanicList.size(), 9);
	}
	
	@Test
	public void findMechanicByName() {
		MechanicAction mechanicAction = new MechanicAction();
		Mechanic mechanic = mechanicAction.findMechanicByName("John", "Doe");
		assertNotNull(mechanic);
	}
	
	@Test
	public void findMechanicById() {
		MechanicAction mechanicAction = new MechanicAction();
		Mechanic mechanic = mechanicAction.findMechanicById("1");
		assertNotNull(mechanic);
	}

}
