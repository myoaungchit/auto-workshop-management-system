package test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import action.RepairRequestAction;
import model.RepairRequest;

public class RepairRequestActionTest {

	@Test
	public void addNewRepairRequest() {
		RepairRequestAction repairRequestAction = new RepairRequestAction();
		RepairRequest repairRequest = new RepairRequest("R0008", "New Exhaust", "Jeep", "SG098674", "02/04/2016", "8");
		List<RepairRequest> repairRequestList = repairRequestAction.addNewRepairRequest(repairRequest);
		assertEquals(repairRequestList.size(), 8);
	}

}
