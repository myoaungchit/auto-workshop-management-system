package test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import action.RepairJobAction;
import model.RepairJob;

public class RepairJobActionTest {

	@Test
	public void addNewRepairJob() {
		RepairJobAction repairJobAction = new RepairJobAction();
		RepairJob repairJob = new RepairJob("J0001", "Pending", "R0001", "New Exhaust", "Ford", "SG12345", "01/04/2016", "1", "2");
		List<RepairJob> repairJobList = repairJobAction.addNewRepairJob(repairJob);
		assertEquals(repairJobList.get(0).getStatus(), "Assigned");
	}

}
