package test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import action.ManagerAction;
import model.Stock;

public class ManagerActionTest {

	@Test
	public void addNewStock() {
		ManagerAction managerAction = new ManagerAction();
		Stock stock = new Stock("S0011", "Water Pump", "4");
		List<Stock> stockList = managerAction.addNewStock(stock);
		assertEquals(stockList.size(), 11);
	}
	
	@Test
	public void updateStock() {
		ManagerAction managerAction = new ManagerAction();
		Stock stock = new Stock("S0001", "Water Pump", "4");
		List<Stock> stockList = managerAction.updateStock(stock);
		assertEquals(stockList.get(0).getQuantity(), "4");
	}
	
	@Test
	public void deleteStock() {
		ManagerAction managerAction = new ManagerAction();
		List<Stock> stockList = managerAction.deleteStock("S0001");
		assertEquals(stockList.size(),9);
	}

}
