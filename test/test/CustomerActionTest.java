package test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import action.CustomerAction;
import model.Customer;

public class CustomerActionTest {

	@Test
	public void addNewCustomer() {
		CustomerAction customerAction = new CustomerAction();
		Customer customer = new Customer("11", "Ryan", "Luke", "651234567", "ryanluck@gmail.com", "Blk54, Robinson Road, Singapore");
		List<Customer> customerList = customerAction.addNewCustomer(customer);
		assertEquals(customerList.size(), 11);
	}
	
	@Test
	public void updateCustomer() {
		CustomerAction customerAction = new CustomerAction();
		Customer customer = new Customer("1", "Ryan", "Luke", "651234567", "ryanluck@gmail.com", "Blk54, Robinson Road, Singapore");
		List<Customer> customerList = customerAction.updateCustomer(customer);
		assertEquals(customerList.get(0).getFirstName(), "Ryan");
	}
	
	@Test
	public void deleteCustomer() {
		CustomerAction customerAction = new CustomerAction();
		List<Customer> customerList = customerAction.deleteCustomer("1");
		assertEquals(customerList.size(), 9);
	}
	
	@Test
	public void findCustomerByName() {
		CustomerAction customerAction = new CustomerAction();
		Customer customer = customerAction.findCustomerByName("John", "Doe");
		assertNotNull(customer);
	}
	
	@Test
	public void findCustomerById() {
		CustomerAction customerAction = new CustomerAction();
		Customer customer = customerAction.findCustomerById("1");
		assertNotNull(customer);
	}

}
