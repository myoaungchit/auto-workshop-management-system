package model;

public class RepairRequest {
	private String repairRequestId;
	private String repairType;
	private String vehicleType;
	private String vehicleNo;
	private String repairRequestDate;
	private String customerId;

	public RepairRequest(String repairRequestId, String repairType, String vehicleType, String vehicleNo,
			String repairRequestDate, String customerId) {
		super();
		this.repairRequestId = repairRequestId;
		this.repairType = repairType;
		this.vehicleType = vehicleType;
		this.vehicleNo = vehicleNo;
		this.repairRequestDate = repairRequestDate;
		this.customerId = customerId;
	}

	public String getRepairRequestId() {
		return repairRequestId;
	}

	public void setRepairRequestId(String repairRequestId) {
		this.repairRequestId = repairRequestId;
	}

	public String getRepairType() {
		return repairType;
	}

	public void setRepairType(String repairType) {
		this.repairType = repairType;
	}

	public String getRepairRequestDate() {
		return repairRequestDate;
	}

	public void setRepairRequestDate(String repairRequestDate) {
		this.repairRequestDate = repairRequestDate;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

}
