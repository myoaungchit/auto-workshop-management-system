package model;

public class RepairJob {
	private String repairJobId;
	private String status;
	private String repairRequestId;
	private String repairType;
	private String vehicleType;
	private String vehicleNo;
	private String repairRequestDate;
	private String customerId;
	private String mechanicId;

	public RepairJob() {
	}

	public RepairJob(String repairJobId, String status, String repairRequestId, String repairType, String vehicleType,
			String vehicleNo, String repairRequestDate, String customerId, String mechanicId) {
		super();
		this.repairJobId = repairJobId;
		this.status = status;
		this.repairRequestId = repairRequestId;
		this.repairType = repairType;
		this.vehicleType = vehicleType;
		this.vehicleNo = vehicleNo;
		this.repairRequestDate = repairRequestDate;
		this.customerId = customerId;
		this.mechanicId = mechanicId;
	}

	public String getRepairJobId() {
		return repairJobId;
	}

	public void setRepairJobId(String repairJobId) {
		this.repairJobId = repairJobId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRepairRequestId() {
		return repairRequestId;
	}

	public void setRepairRequestId(String repairRequestId) {
		this.repairRequestId = repairRequestId;
	}

	public String getMechanicId() {
		return mechanicId;
	}

	public void setMechanicId(String mechanicId) {
		this.mechanicId = mechanicId;
	}

	public String getRepairType() {
		return repairType;
	}

	public void setRepairType(String repairType) {
		this.repairType = repairType;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public String getRepairRequestDate() {
		return repairRequestDate;
	}

	public void setRepairRequestDate(String repairRequestDate) {
		this.repairRequestDate = repairRequestDate;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

}
