package model;

public class Mechanic {
	private String mechanicId;
	private String firstName;
	private String lastName;
	private String phoneNo;
	private String email;
	private String address;
	private String mechanicType;

	public Mechanic() {
	}

	public Mechanic(String mechanicId, String firstName, String lastName, String phoneNo, String email, String address,
			String mechanicType) {
		super();
		this.mechanicId = mechanicId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNo = phoneNo;
		this.email = email;
		this.address = address;
		this.mechanicType = mechanicType;
	}

	public String getMechanicId() {
		return mechanicId;
	}

	public void setMechanicId(String mechanicId) {
		this.mechanicId = mechanicId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMechanicType() {
		return mechanicType;
	}

	public void setMechanicType(String mechanicType) {
		this.mechanicType = mechanicType;
	}

}
