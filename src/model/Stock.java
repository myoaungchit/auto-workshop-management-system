package model;

public class Stock {
	private String stockId;
	private String part;
	private String quantity;

	public Stock() {
	}

	public Stock(String stockId, String part, String quantity) {
		super();
		this.stockId = stockId;
		this.part = part;
		this.quantity = quantity;
	}

	public String getStockId() {
		return stockId;
	}

	public void setStockId(String stockId) {
		this.stockId = stockId;
	}

	public String getPart() {
		return part;
	}

	public void setPart(String part) {
		this.part = part;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

}
