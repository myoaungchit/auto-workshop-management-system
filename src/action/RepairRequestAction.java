package action;

import java.util.ArrayList;
import java.util.List;

import model.RepairRequest;

public class RepairRequestAction {
	private RepairRequest repairRequest;

	public RepairRequest getRepairRequest() {
		return repairRequest;
	}

	public void setRepairRequest(RepairRequest repairRequest) {
		this.repairRequest = repairRequest;
	}
	
	public List<RepairRequest> prepareRepairRquest() {
		List<RepairRequest> repairRequestList = new ArrayList<RepairRequest>();
		RepairRequest request1 = new RepairRequest("R0001", "New Exhaust", "Toyota", "SG12345", "01/04/2016", "1");
		RepairRequest request2 = new RepairRequest("R0002", "New Tyres", "BMW", "SG222222", "20/03/2016", "2");
		RepairRequest request3 = new RepairRequest("R0003", "New Tyres", "Ford", "SG43567", "10/04/2016", "3");
		RepairRequest request4 = new RepairRequest("R0004", "New Engine", "Toyota", "SG876543", "11/04/2016", "4");
		RepairRequest request5 = new RepairRequest("R0005", "New Exhaust", "Jeep", "SG132980", "13/04/2016", "5");
		RepairRequest request6 = new RepairRequest("R0006", "New Engine", "Honda", "SG998856", "13/03/2016", "6");
		RepairRequest request7 = new RepairRequest("R0007", "New Exhaust", "Jeep", "SG098674", "02/04/2016", "7");
		repairRequestList.add(request1);
		repairRequestList.add(request2);
		repairRequestList.add(request3);
		repairRequestList.add(request4);
		repairRequestList.add(request5);
		repairRequestList.add(request6);
		repairRequestList.add(request7);
		return repairRequestList;
	}
	
	public List<RepairRequest> addNewRepairRequest(RepairRequest repairRequest) {
		List<RepairRequest> repairRequestList = prepareRepairRquest();
		repairRequestList.add(repairRequest);
		return repairRequestList;
	}
}
