package action;

import java.util.ArrayList;
import java.util.List;

import model.RepairJob;

public class RepairJobAction {

	private RepairJob repairJob;

	public List<RepairJob> prepareRepairJobList() {
		List<RepairJob> repairJobList = new ArrayList<RepairJob>();
		RepairJob job1 = new RepairJob("", "Completed", "R0001", "New Exhaust", "Ford", "SG12345", "01/02/2016", "1", "");
		RepairJob job2 = new RepairJob("", "Pending", "R0002", "New Tyres", "BMW", "SG222222", "23/03/2016", "2", "");
		RepairJob job3 = new RepairJob("", "Pending", "R0003", "New Tyres", "Ford", "SG43567", "12/04/2016", "3", "");
		RepairJob job4 = new RepairJob("", "Assigned", "R0004", "New Engine", "Toyota", "SG876543", "17/04/2016", "4",
				"");
		RepairJob job5 = new RepairJob("", "Pending", "R0005", "New Exhaust", "Jeep", "SG132980", "03/04/2016", "5", "");
		RepairJob job6 = new RepairJob("", "Assigned", "R0006", "New Engine", "Honda", "SG998856", "30/03/2016", "6",
				"");
		RepairJob job7 = new RepairJob("", "Completed", "R0007", "New Exhaust", "Jeep", "SG098674", "22/04/2016", "7",
				"");

		repairJobList.add(job1);
		repairJobList.add(job2);
		repairJobList.add(job3);
		repairJobList.add(job4);
		repairJobList.add(job5);
		repairJobList.add(job6);
		repairJobList.add(job7);
		return repairJobList;
	}

	public List<RepairJob> addNewRepairJob(RepairJob repairJob) {
		List<RepairJob> repairJobList = prepareRepairJobList();
		for (int i = 0; i < repairJobList.size(); i++) {
			if (repairJob.getRepairRequestId().equals(repairJobList.get(i).getRepairRequestId())) {
				repairJobList.get(i).setStatus("Assigned");
				repairJobList.get(i).setRepairType(repairJobList.get(i).getRepairType());
				repairJobList.get(i).setVehicleType(repairJobList.get(i).getVehicleType());
				repairJobList.get(i).setVehicleNo(repairJobList.get(i).getVehicleNo());
				repairJobList.get(i).setRepairRequestDate(repairJobList.get(i).getRepairRequestDate());
				repairJobList.get(i).setCustomerId(repairJobList.get(i).getCustomerId());
				repairJobList.get(i).setMechanicId(repairJobList.get(i).getMechanicId());
			}
		}

		return repairJobList;
	}

	public RepairJob getRepairJob() {
		return repairJob;
	}

	public void setRepairJob(RepairJob repairJob) {
		this.repairJob = repairJob;
	}

}
