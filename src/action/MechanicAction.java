package action;

import java.util.ArrayList;
import java.util.List;

import model.Mechanic;

public class MechanicAction {
	private Mechanic mechanic;

	public Mechanic getMechanic() {
		return mechanic;
	}

	public void setMechanic(Mechanic mechanic) {
		this.mechanic = mechanic;
	}

	public List<Mechanic> preapreMechanicList() {
		List<Mechanic> mechanicList = new ArrayList<Mechanic>();
		Mechanic mechanic1 = new Mechanic("1", "Marie", "Jenkins", "555", "mjenkins@gmail.com",
				"123, Robison Rd, Singapore", "Trainee");
		Mechanic mechanic2 = new Mechanic("2", "Beverly", "Richardson", "222", "brichard@hotmail.com",
				"90, Jhar St, Singapore", "Master");
		Mechanic mechanic3 = new Mechanic("3", "Shawn", "Cox", "666", "shawncox@gmail.com",
				"1, Robin Rd, Singapore", "Workman");
		Mechanic mechanic4 = new Mechanic("4", "Mary", "Brown", "888", "mary@yahoo.com",
				"44, Main Rd, Singapore", "Workman");
		Mechanic mechanic5 = new Mechanic("5", "Bill", "Gates", "655", "bgates@gmail.com",
				"32, Laser Drive, Singapore", "Trainee");
		Mechanic mechanic6 = new Mechanic("6", "London", "Luu", "557", "lluu@gmail.com",
				"68, Bank St, Singapore", "Master");
		Mechanic mechanic7 = new Mechanic("7", "Jay", "Donut", "781", "jdonut@gmail.com",
				"129, Rock Rd, Singapore", "Trainee");
		Mechanic mechanic8 = new Mechanic("8", "Bruce", "Lee", "653", "b.lee@gmail.com",
				"90, Kung Fu St, Singapore", "Master");
		Mechanic mechanic9 = new Mechanic("9", "Ben", "Bean", "656", "bbean@hotmail.com",
				"17, Main Rd, Singapore", "Workman");
		Mechanic mechanic10 = new Mechanic("10", "Rick", "Ong", "432", "rick.ong@gmail.com",
				"87, Dawn St, Singapore", "Workman");
		mechanicList.add(mechanic1);
		mechanicList.add(mechanic2);
		mechanicList.add(mechanic3);
		mechanicList.add(mechanic4);
		mechanicList.add(mechanic5);
		mechanicList.add(mechanic6);
		mechanicList.add(mechanic7);
		mechanicList.add(mechanic8);
		mechanicList.add(mechanic9);
		mechanicList.add(mechanic10);
		return mechanicList;
	}

	public List<Mechanic> addNewMechanic(Mechanic mechanic2) {
		List<Mechanic> mechanicList = preapreMechanicList();
		mechanicList.add(mechanic2);
		return mechanicList;
	}

	public List<Mechanic> updateMechanic(Mechanic mechanic2) {
		List<Mechanic> mechanicList = preapreMechanicList();
		for (int i = 0; i < mechanicList.size(); i++) {
			if (mechanic2.getMechanicId().equals(mechanicList.get(i).getMechanicId())) {
				mechanicList.set(i, mechanic2);
			}
		}
		return mechanicList;
	}

	public List<Mechanic> deleteMechanic(String mechanicId) {
		List<Mechanic> mechanicList = preapreMechanicList();
		for (int i = 0; i < mechanicList.size(); i++) {
			if (mechanicId.equals(mechanicList.get(i).getMechanicId())) {
				mechanicList.remove(i);
			}
		}
		return mechanicList;
	}

	public Mechanic findMechanicByName(String firstName, String lastName) {
		List<Mechanic> mechanicList = preapreMechanicList();
		Mechanic mechanic = null;
		for (int i = 0; i < mechanicList.size(); i++) {
			if (mechanicList.get(i).getFirstName().equals(firstName)
					&& mechanicList.get(i).getLastName().equals(lastName)) {
				mechanic = mechanicList.get(i);
			}
		}
		return mechanic;

	}

	public Mechanic findMechanicById(String mechanicId) {
		List<Mechanic> mechanicList = preapreMechanicList();
		Mechanic mechanic = null;
		for (int i = 0; i < mechanicList.size(); i++) {
			if (mechanicList.get(i).getMechanicId().equals(mechanicId)) {
				mechanic = mechanicList.get(i);
			}
		}
		return mechanic;

	}
}
