package action;

import java.util.ArrayList;
import java.util.List;

import model.Manager;
import model.Stock;

public class ManagerAction {
	private Manager manager;

	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

	public List<Stock> preapreStockList() {
		List<Stock> stockList = new ArrayList<Stock>();
		Stock stock1 = new Stock("S0001", "Battery", "100");
		Stock stock2 = new Stock("S0002", "Starter", "30");
		Stock stock3 = new Stock("S0003", "Alternator", "10");
		Stock stock4 = new Stock("S0004", "Brake Rotor", "1");
		Stock stock5 = new Stock("S0005", "Engine", "6");
		Stock stock6 = new Stock("S0006", "Motor", "42");
		Stock stock7 = new Stock("S0007", "Belt", "10");
		Stock stock8 = new Stock("S0008", "Pump", "12");
		Stock stock9 = new Stock("S0009", "Compressor", "50");
		Stock stock10 = new Stock("S0010", "Headlight", "102");
		stockList.add(stock1);
		stockList.add(stock2);
		stockList.add(stock3);
		stockList.add(stock4);
		stockList.add(stock5);
		stockList.add(stock6);
		stockList.add(stock7);
		stockList.add(stock8);
		stockList.add(stock9);
		stockList.add(stock10);
		return stockList;
	}

	public List<Stock> addNewStock(Stock stock) {
		List<Stock> stockList = preapreStockList();
		stockList.add(stock);
		return stockList;
	}

	public List<Stock> updateStock(Stock stock) {
		List<Stock> stockList = preapreStockList();
		for (int i = 0; i < stockList.size(); i++) {
			if (stock.getStockId().equals(stockList.get(i).getStockId())) {
				stockList.set(i, stock);
			}
		}
		return stockList;
	}

	public List<Stock> deleteStock(String stockId) {
		List<Stock> stockList = preapreStockList();
		for (int i = 0; i < stockList.size(); i++) {
			if (stockId.equals(stockList.get(i).getStockId())) {
				stockList.remove(i);
			}
		}
		return stockList;
	}
}
