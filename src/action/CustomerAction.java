package action;

import java.util.ArrayList;
import java.util.List;

import model.Customer;

public class CustomerAction {
	private Customer customer;

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<Customer> preapreCustomerList() {
		List<Customer> customerList = new ArrayList<Customer>();
		Customer cust1 = new Customer("1", "Jay", "Atwood", "54352317", "jatwood@gmail.com",
				"#04-08, Yong Siak St, Singapore");
		Customer cust2 = new Customer("2", "Erica", "Lang", "46127423", "erica.tang@gmail.com",
				"#08-04, Yang Zee St, Singapore");
		Customer cust3 = new Customer("3", "Gary", "Ng", "44135411", "gng@mail.com",
				"195, Orchard Rd, Singapore");
		Customer cust4 = new Customer("4", "Mandy", "Ong", "56948180", "mandyong@yahoo.com",
				"#09-100, Mandai Rd, Malaysia");
		Customer cust5 = new Customer("5", "John", "Doe", "22299701", "jdoe@gmail.com",
				"32, Infinity Drive, Singapore");
		Customer cust6 = new Customer("6", "Kim", "Chi", "95526710", "kc@icloud.com",
				"68, Bane St, Singapore");
		Customer cust7 = new Customer("7", "Paul", "Walker", "65432781", "pwalker@gmail.com",
				"9, Fast & Furious  Rd, Singapore");
		Customer cust8 = new Customer("8", "Patrick", "Green", "97264534", "patrick.green@gmail.com",
				"34, Chimes St, Singapore");
		Customer cust9 = new Customer("9", "Mr", "Bean", "96414193", "mrbean@mail.com",
				"90,  Joker St, Singapore");
		Customer cust10 = new Customer("10", "Mark", "Zuck", "65765432", "mzuck@facebook.com",
				"1, Friendly Lane, Singapore");
		customerList.add(cust1);
		customerList.add(cust2);
		customerList.add(cust3);
		customerList.add(cust4);
		customerList.add(cust5);
		customerList.add(cust6);
		customerList.add(cust7);
		customerList.add(cust8);
		customerList.add(cust9);
		customerList.add(cust10);
		return customerList;
	}

	public List<Customer> addNewCustomer(Customer customer) {
		List<Customer> customerList = preapreCustomerList();
		customerList.add(customer);
		return customerList;
	}

	public List<Customer> updateCustomer(Customer customer) {
		List<Customer> customerList = preapreCustomerList();
		for (int i = 0; i < customerList.size(); i++) {
			if (customer.getCustomerId().equals(customerList.get(i).getCustomerId())) {
				customerList.set(i, customer);
			}
		}
		return customerList;
	}

	public List<Customer> deleteCustomer(String customerId) {
		List<Customer> customerList = preapreCustomerList();
		for (int i = 0; i < customerList.size(); i++) {
			if (customerId.equals(customerList.get(i).getCustomerId())) {
				customerList.remove(i);
			}
		}
		return customerList;
	}

	public Customer findCustomerByName(String firstName, String lastName) {
		List<Customer> customerList = preapreCustomerList();
		Customer customer = null;
		for (int i = 0; i < customerList.size(); i++) {
			if (customerList.get(i).getFirstName().equals(firstName)
					&& customerList.get(i).getLastName().equals(lastName)) {
				customer = customerList.get(i);
			}
		}
		return customer;

	}
	
	public Customer findCustomerById(String customerId) {
		List<Customer> customerList = preapreCustomerList();
		Customer customer = null;
		for (int i = 0; i < customerList.size(); i++) {
			if (customerList.get(i).getCustomerId().equals(customerId)) {
				customer = customerList.get(i);
			}
		}
		return customer;

	}
}
