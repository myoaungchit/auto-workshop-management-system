package gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import action.CustomerAction;
import model.Customer;

public class CustomerUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6809813320859748492L;
	private JPanel contentPane;
	private JTable table;
	private CustomerAction custAction = new CustomerAction();
	private JLabel lblNewLabel;
	private JLabel lblCustomerId;
	private JLabel lblNewLabel_1;
	private JLabel lblLastName;
	private JLabel lblPhoneNumber;
	private JLabel lblEmail;
	private JLabel lblAddress;
	private JTextField textFirstName;
	private JTextField textLastName;
	private JTextField textPhoneNo;
	private JTextField textEmail;
	private JTextField textCustomerId;;
	private JTextArea textAreaAddress;

	/**
	 * Create the frame.
	 */
	public CustomerUI() {
		setTitle("Customer");
		setJMenuBar(MenuAction.initMenu(this));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setBounds(5, 5, 1280, 1024);
		contentPane = new JPanel();
		contentPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblCustomerList = new JLabel("Customer List");
		lblCustomerList.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblCustomerList.setBounds(21, 11, 133, 34);
		contentPane.add(lblCustomerList);

		lblNewLabel = new JLabel("Customer Record");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel.setBounds(10, 518, 225, 34);
		contentPane.add(lblNewLabel);

		JPanel panel = new JPanel();
		panel.setBounds(10, 563, 399, 391);
		contentPane.add(panel);
		panel.setLayout(null);

		lblCustomerId = new JLabel("Customer ID");
		lblCustomerId.setBounds(10, 11, 107, 25);
		panel.add(lblCustomerId);

		textCustomerId = new JTextField();
		textCustomerId.setBounds(156, 13, 220, 25);
		panel.add(textCustomerId);
		textCustomerId.setColumns(10);

		lblNewLabel_1 = new JLabel("First Name");
		lblNewLabel_1.setBounds(10, 48, 107, 25);
		panel.add(lblNewLabel_1);

		textFirstName = new JTextField();
		textFirstName.setBounds(156, 48, 220, 25);
		panel.add(textFirstName);
		textFirstName.setColumns(10);

		lblLastName = new JLabel("Last Name");
		lblLastName.setBounds(10, 84, 82, 25);
		panel.add(lblLastName);

		textLastName = new JTextField();
		textLastName.setBounds(156, 85, 220, 24);
		panel.add(textLastName);
		textLastName.setColumns(10);

		lblPhoneNumber = new JLabel("Phone Number");
		lblPhoneNumber.setBounds(10, 120, 107, 25);
		panel.add(lblPhoneNumber);

		textPhoneNo = new JTextField();
		textPhoneNo.setBounds(156, 120, 220, 25);
		panel.add(textPhoneNo);
		textPhoneNo.setColumns(10);

		lblEmail = new JLabel("Email");
		lblEmail.setBounds(10, 156, 55, 25);
		panel.add(lblEmail);

		textEmail = new JTextField();
		textEmail.setBounds(156, 156, 220, 25);
		panel.add(textEmail);
		textEmail.setColumns(10);

		lblAddress = new JLabel("Address");
		lblAddress.setBounds(10, 192, 82, 34);
		panel.add(lblAddress);

		textAreaAddress = new JTextArea();
		textAreaAddress.setBounds(156, 198, 220, 96);
		panel.add(textAreaAddress);
		textAreaAddress.setFont(new Font("Tahoma", Font.PLAIN, 13));

		table = new JTable();
		table.setBounds(0, 0, 1, 1);

		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int selectedRow = table.getSelectedRow();
				String customerId = table.getModel().getValueAt(selectedRow, 0).toString();
				textCustomerId.setText(customerId);
				List<Customer> customerList = custAction.preapreCustomerList();

				for (int i = 0; i < customerList.size(); i++) {
					if (customerList.get(i).getCustomerId().equals(customerId)) {
						textFirstName.setText(customerList.get(i).getFirstName());
						textLastName.setText(customerList.get(i).getLastName());
						textPhoneNo.setText(customerList.get(i).getPhoneNo());
						textEmail.setText(customerList.get(i).getEmail());
						textAreaAddress.setText(customerList.get(i).getAddress());
					}
				}
			}
		});

		displayData(custAction.preapreCustomerList());

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 68, 1244, 419);
		contentPane.add(scrollPane);

		JButton btnAdd = new JButton("New Record");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Customer customer = new Customer(textCustomerId.getText(), textFirstName.getText(),
						textLastName.getText(), textPhoneNo.getText(), textEmail.getText(), textAreaAddress.getText());
				List<Customer> customerList = custAction.addNewCustomer(customer);
				JOptionPane.showMessageDialog(null, "New record has inserted successfully.");
				displayData(customerList);
				clear();
			}
		});
		btnAdd.setBounds(10, 333, 107, 34);
		panel.add(btnAdd);

		JButton btnNewButton = new JButton("Update Record");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Customer customer = new Customer(textCustomerId.getText(), textFirstName.getText(),
						textLastName.getText(), textPhoneNo.getText(), textEmail.getText(), textAreaAddress.getText());
				List<Customer> customerList = custAction.updateCustomer(customer);
				JOptionPane.showMessageDialog(null, "The record has updated successfully.");
				displayData(customerList);
				clear();
			}
		});
		btnNewButton.setBounds(134, 333, 130, 34);
		panel.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("Delete Record");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selectedRow = table.getSelectedRow();
				String customerId = table.getModel().getValueAt(selectedRow, 0).toString();
				List<Customer> customerList = custAction.deleteCustomer(customerId);
				JOptionPane.showMessageDialog(null, "The record has deleted successfully.");
				displayData(customerList);
				clear();
			}
		});
		btnNewButton_1.setBounds(274, 333, 115, 34);
		panel.add(btnNewButton_1);

	}

	private void displayData(List customerList) {
		DefaultTableModel dataModel = new DefaultTableModel() {
			// setting the jtable read only
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		// setting the column name
		Vector<String> header = new Vector<String>();
		header.add("Customer ID");
		header.add("First Name");
		header.add("Last Name");
		header.add("Phone No");
		header.add("Email");
		header.add("Address");

		dataModel.setColumnIdentifiers(header);
		if (customerList == null) {
			this.table.setModel(dataModel);
			return;
		}

		ListIterator<Customer> lstrg = customerList.listIterator();
		// populating the tablemodel
		while (lstrg.hasNext()) {
			Customer customer = lstrg.next();
			Vector<String> column = new Vector<String>();
			column.add(customer.getCustomerId());
			column.add(customer.getFirstName());
			column.add(customer.getLastName());
			column.add(customer.getPhoneNo());
			column.add(customer.getEmail());
			column.add(customer.getAddress());
			dataModel.addRow(column);
		}
		// binding the jtable to the model
		this.table.setModel(dataModel);
	}

	private void clear() {
		textFirstName.setText("");
		textLastName.setText("");
		textPhoneNo.setText("");
		textEmail.setText("");
		textCustomerId.setText("");
		textAreaAddress.setText("");
	}
}
