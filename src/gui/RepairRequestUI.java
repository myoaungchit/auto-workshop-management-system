package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import action.CustomerAction;
import action.RepairRequestAction;
import model.Customer;
import model.RepairRequest;

public class RepairRequestUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9214358761958361491L;
	private JPanel contentPane;
	private JTextField textRequestId;
	private JTextField textVehicleNo;
	private JTextField textVehicleType;
	private JTextField textDate;
	private JTextField textFirstName;
	private JComboBox<String> cboRepairType;
	private JTable table;
	private RepairRequestAction repairRequestAction = new RepairRequestAction();
	private CustomerAction custAction = new CustomerAction();
	private JTextField textLastName;

	

	/**
	 * Create the frame.
	 */
	public RepairRequestUI() {
		setTitle("Repair Request");
		setJMenuBar(MenuAction.initMenu(this));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setBounds(5, 5, 1280, 1024);
		contentPane = new JPanel();
		contentPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Repair Request");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel.setBounds(10, 26, 204, 31);
		contentPane.add(lblNewLabel);
		
		JPanel panel = new JPanel();
		panel.setBackground(UIManager.getColor("Button.background"));
		panel.setBounds(10, 66, 667, 438);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblRepair = new JLabel("Repair Type");
		lblRepair.setBounds(10, 62, 92, 33);
		panel.add(lblRepair);
		
		cboRepairType = new JComboBox<String>();
		cboRepairType.addItem("New Exhaust");
		cboRepairType.addItem("New Tyres");
		cboRepairType.addItem("New Engine");
		cboRepairType.setBounds(165, 63, 268, 32);
		panel.add(cboRepairType);
		
		JLabel lblCustomer = new JLabel("Request ID");
		lblCustomer.setBounds(10, 23, 66, 28);
		panel.add(lblCustomer);
		
		textRequestId = new JTextField();
		textRequestId.setBounds(165, 20, 268, 32);
		panel.add(textRequestId);
		textRequestId.setColumns(10);
		
		JLabel lblVehicleNo = new JLabel("Vehicle No");
		lblVehicleNo.setBounds(10, 112, 79, 31);
		panel.add(lblVehicleNo);
		
		textVehicleNo = new JTextField();
		textVehicleNo.setBounds(165, 111, 268, 33);
		panel.add(textVehicleNo);
		textVehicleNo.setColumns(10);
		
		JLabel lblVehicleType = new JLabel("Vehicle Type");
		lblVehicleType.setBounds(10, 153, 92, 36);
		panel.add(lblVehicleType);
		
		textVehicleType = new JTextField();
		textVehicleType.setBounds(165, 155, 268, 33);
		panel.add(textVehicleType);
		textVehicleType.setColumns(10);
		
		JLabel lblDate = new JLabel("Date (dd/mm/yyyy)");
		lblDate.setBounds(10, 200, 116, 28);
		panel.add(lblDate);
		
		textDate = new JTextField();
		textDate.setBounds(165, 199, 268, 36);
		panel.add(textDate);
		textDate.setColumns(10);
		
		JLabel lblCustomer_1 = new JLabel("First Name");
		lblCustomer_1.setBounds(10, 248, 66, 28);
		panel.add(lblCustomer_1);
		
		textFirstName = new JTextField();
		textFirstName.setBounds(165, 246, 268, 33);
		panel.add(textFirstName);
		textFirstName.setColumns(10);
		
		table = new JTable();
		table.setBounds(0, 0, 1, 1);

		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int selectedRow = table.getSelectedRow();
				String requestId = table.getModel().getValueAt(selectedRow, 0).toString();
				textRequestId.setText(requestId);
				List<RepairRequest> repairRequestList = repairRequestAction.prepareRepairRquest();

				for (int i = 0; i < repairRequestList.size(); i++) {
					if (repairRequestList.get(i).getRepairRequestId().equals(requestId)) {
						cboRepairType.setSelectedItem(repairRequestList.get(i).getRepairType());
						textVehicleType.setText(repairRequestList.get(i).getVehicleType());
						textVehicleNo.setText(repairRequestList.get(i).getVehicleNo());
						textDate.setText(repairRequestList.get(i).getRepairRequestDate());						
						Customer customer = custAction.findCustomerById(repairRequestList.get(i).getCustomerId());
						textFirstName.setText(customer.getFirstName());
						textLastName.setText(customer.getLastName());
					}
				}
			}
		});

		displayData(repairRequestAction.prepareRepairRquest());

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 515, 1244, 419);
		contentPane.add(scrollPane);
		
		
		JButton btnNewButton = new JButton("Submit");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String repairType = String.valueOf(cboRepairType.getSelectedItem());
				Customer customer = custAction.findCustomerByName(textFirstName.getText(), textLastName.getText());
				if(customer != null) {
					RepairRequest repairRequest = new RepairRequest(textRequestId.getText(), repairType,
							textVehicleType.getText(), textVehicleNo.getText(), textDate.getText(),customer.getCustomerId());
					List<RepairRequest> repairRequestList = repairRequestAction.addNewRepairRequest(repairRequest);
					JOptionPane.showMessageDialog(null, "New record has inserted successfully.");
					displayData(repairRequestList);
				} else {
					JOptionPane.showMessageDialog(null, "This customer is not in the customer list. Please insert customer record first.");
					displayData(repairRequestAction.prepareRepairRquest());
				}
				
				
			}
		});
		btnNewButton.setBounds(10, 371, 160, 41);
		panel.add(btnNewButton);
		
		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setBounds(10, 298, 92, 28);
		panel.add(lblLastName);
		
		textLastName = new JTextField();
		textLastName.setBounds(164, 290, 269, 33);
		panel.add(textLastName);
		textLastName.setColumns(10);
	}
	
	private void displayData(List repairRequestList) {
		DefaultTableModel dataModel = new DefaultTableModel() {
			// setting the jtable read only
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		// setting the column name
		Vector<String> header = new Vector<String>();
		header.add("Repair Request ID");
		header.add("Repair Type");
		header.add("Vehicle Type");
		header.add("Vehicle No");
		header.add("Request Date");
		header.add("Customer");

		dataModel.setColumnIdentifiers(header);
		if (repairRequestList == null) {
			this.table.setModel(dataModel);
			return;
		}
		
		

		ListIterator<RepairRequest> lstrg = repairRequestList.listIterator();
		// populating the tablemodel
		while (lstrg.hasNext()) {
			RepairRequest repairRequest = lstrg.next();
			Vector<String> column = new Vector<String>();
			column.add(repairRequest.getRepairRequestId());
			column.add(repairRequest.getRepairType());
			column.add(repairRequest.getVehicleType());
			column.add(repairRequest.getVehicleNo());
			column.add(repairRequest.getRepairRequestDate());
			Customer customer = custAction.findCustomerById(repairRequest.getCustomerId());
			column.add(customer.getFirstName() + " " + customer.getLastName());
			dataModel.addRow(column);
		}
		// binding the jtable to the model
		this.table.setModel(dataModel);
	}
}
