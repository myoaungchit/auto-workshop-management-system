package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import action.ManagerAction;
import model.Stock;

public class StockUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2875927819423652923L;
	private JPanel contentPane;
	private JTable table;
	private ManagerAction managerAction = new ManagerAction();
	private JLabel lblNewLabel;
	private JLabel lblStockId;
	private JLabel lblPart;
	private JLabel lblQuantity;
	private JTextField textPart;
	private JTextField textQuantity;
	private JTextField textStockId;;

	/**
	 * Create the frame.
	 */
	public StockUI() {
		setTitle("Customer");
		setJMenuBar(MenuAction.initMenu(this));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setBounds(5, 5, 1280, 1024);
		contentPane = new JPanel();
		contentPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblCustomerList = new JLabel("Stock List");
		lblCustomerList.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblCustomerList.setBounds(21, 11, 133, 34);
		contentPane.add(lblCustomerList);

		lblNewLabel = new JLabel("Stock Record");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel.setBounds(10, 650, 225, 34);
		contentPane.add(lblNewLabel);

		JPanel panel = new JPanel();
		panel.setBounds(10, 717, 399, 220);
		contentPane.add(panel);
		panel.setLayout(null);

		lblStockId = new JLabel("Stock ID");
		lblStockId.setBounds(10, 11, 107, 25);
		panel.add(lblStockId);

		textStockId = new JTextField();
		textStockId.setBounds(156, 13, 220, 25);
		panel.add(textStockId);
		textStockId.setColumns(10);

		lblPart = new JLabel("Part");
		lblPart.setBounds(10, 48, 107, 25);
		panel.add(lblPart);

		textPart = new JTextField();
		textPart.setBounds(156, 48, 220, 25);
		panel.add(textPart);
		textPart.setColumns(10);

		lblQuantity = new JLabel("Quantity");
		lblQuantity.setBounds(10, 84, 82, 25);
		panel.add(lblQuantity);

		textQuantity = new JTextField();
		textQuantity.setBounds(156, 85, 220, 24);
		panel.add(textQuantity);
		textQuantity.setColumns(10);

		table = new JTable();
		table.setBounds(0, 0, 1, 1);

		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int selectedRow = table.getSelectedRow();
				String stockId = table.getModel().getValueAt(selectedRow, 0).toString();
				textStockId.setText(stockId);
				List<Stock> stockList = managerAction.preapreStockList();

				for (int i = 0; i < stockList.size(); i++) {
					if (stockList.get(i).getStockId().equals(stockId)) {
						textPart.setText(stockList.get(i).getPart());
						textQuantity.setText(stockList.get(i).getQuantity());
					}
				}
			}
		});

		displayData(managerAction.preapreStockList());

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 68, 1244, 571);
		contentPane.add(scrollPane);

		JButton btnAdd = new JButton("New Record");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Stock stock = new Stock(textStockId.getText(), textPart.getText(), textQuantity.getText());
				List<Stock> stockList = managerAction.addNewStock(stock);
				JOptionPane.showMessageDialog(null, "New record has inserted successfully.");
				displayData(stockList);
				clear();
			}
		});
		btnAdd.setBounds(10, 157, 107, 34);
		panel.add(btnAdd);

		JButton btnNewButton = new JButton("Update Record");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Stock stock = new Stock(textStockId.getText(), textPart.getText(), textQuantity.getText());
				List<Stock> stockList = managerAction.updateStock(stock);
				JOptionPane.showMessageDialog(null, "The record has updated successfully.");
				displayData(stockList);
				clear();
			}
		});
		btnNewButton.setBounds(127, 157, 130, 34);
		panel.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("Delete Record");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selectedRow = table.getSelectedRow();
				String stockId = table.getModel().getValueAt(selectedRow, 0).toString();
				List<Stock> stockList = managerAction.deleteStock(stockId);
				JOptionPane.showMessageDialog(null, "The record has deleted successfully.");
				displayData(stockList);
				clear();
			}
		});
		btnNewButton_1.setBounds(267, 157, 115, 34);
		panel.add(btnNewButton_1);

	}

	private void displayData(List<Stock> stockList) {
		DefaultTableModel dataModel = new DefaultTableModel() {
			// setting the jtable read only
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		// setting the column name
		Vector<String> header = new Vector<String>();
		header.add("Stock Id");
		header.add("Part");
		header.add("Quantity");

		dataModel.setColumnIdentifiers(header);
		if (stockList == null) {
			this.table.setModel(dataModel);
			return;
		}

		ListIterator<Stock> lstrg = stockList.listIterator();
		// populating the tablemodel
		while (lstrg.hasNext()) {
			Stock stock = lstrg.next();
			Vector<String> column = new Vector<String>();
			column.add(stock.getStockId());
			column.add(stock.getPart());
			column.add(stock.getQuantity());
			dataModel.addRow(column);
		}
		// binding the jtable to the model
		this.table.setModel(dataModel);
	}

	private void clear() {
		textStockId.setText("");
		textPart.setText("");
		textQuantity.setText("");
	}

}
