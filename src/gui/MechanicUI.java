package gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import action.MechanicAction;
import model.Mechanic;

public class MechanicUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2796487271801162813L;
	private JPanel contentPane;
	private JTable table;
	private MechanicAction mechanicAction = new MechanicAction();
	private JLabel lblNewLabel;
	private JLabel lblMechanicId;
	private JLabel lblFirstName;
	private JLabel lblLastName;
	private JLabel lblPhoneNumber;
	private JLabel lblEmail;
	private JLabel lblAddress;
	private JTextField textFirstName;
	private JTextField textLastName;
	private JTextField textPhoneNo;
	private JTextField textEmail;
	private JTextField textMechanicId;;
	private JTextArea textAreaAddress;
	private JLabel lblMechanicType;
	private JComboBox<String> cboMechanicType;

	/**
	 * Create the frame.
	 */
	public MechanicUI() {
		setTitle("Mechanic");
		setJMenuBar(MenuAction.initMenu(this));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setBounds(5, 5, 1280, 1024);
		contentPane = new JPanel();
		contentPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblCustomerList = new JLabel("Mechanic List");
		lblCustomerList.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblCustomerList.setBounds(21, 11, 133, 34);
		contentPane.add(lblCustomerList);

		lblNewLabel = new JLabel("Mechanic Record");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel.setBounds(10, 460, 225, 34);
		contentPane.add(lblNewLabel);

		JPanel panel = new JPanel();
		panel.setBounds(10, 505, 399, 435);
		contentPane.add(panel);
		panel.setLayout(null);

		lblMechanicId = new JLabel("Mechanic ID");
		lblMechanicId.setBounds(10, 11, 107, 25);
		panel.add(lblMechanicId);

		textMechanicId = new JTextField();
		textMechanicId.setBounds(156, 13, 220, 25);
		panel.add(textMechanicId);
		textMechanicId.setColumns(10);

		lblFirstName = new JLabel("First Name");
		lblFirstName.setBounds(10, 48, 107, 25);
		panel.add(lblFirstName);

		textFirstName = new JTextField();
		textFirstName.setBounds(156, 48, 220, 25);
		panel.add(textFirstName);
		textFirstName.setColumns(10);

		lblLastName = new JLabel("Last Name");
		lblLastName.setBounds(10, 84, 82, 25);
		panel.add(lblLastName);

		textLastName = new JTextField();
		textLastName.setBounds(156, 85, 220, 24);
		panel.add(textLastName);
		textLastName.setColumns(10);

		lblPhoneNumber = new JLabel("Phone Number");
		lblPhoneNumber.setBounds(10, 120, 107, 25);
		panel.add(lblPhoneNumber);

		textPhoneNo = new JTextField();
		textPhoneNo.setBounds(156, 120, 220, 25);
		panel.add(textPhoneNo);
		textPhoneNo.setColumns(10);

		lblEmail = new JLabel("Email");
		lblEmail.setBounds(10, 156, 55, 25);
		panel.add(lblEmail);

		textEmail = new JTextField();
		textEmail.setBounds(156, 156, 220, 25);
		panel.add(textEmail);
		textEmail.setColumns(10);

		lblAddress = new JLabel("Address");
		lblAddress.setBounds(10, 192, 82, 34);
		panel.add(lblAddress);

		textAreaAddress = new JTextArea();
		textAreaAddress.setBounds(156, 198, 220, 96);
		panel.add(textAreaAddress);
		textAreaAddress.setFont(new Font("Tahoma", Font.PLAIN, 13));

		table = new JTable();
		table.setBounds(0, 0, 1, 1);

		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int selectedRow = table.getSelectedRow();
				String mechanicId = table.getModel().getValueAt(selectedRow, 0).toString();
				textMechanicId.setText(mechanicId);
				List<Mechanic> mechanicList = mechanicAction.preapreMechanicList();

				for (int i = 0; i < mechanicList.size(); i++) {
					if (mechanicList.get(i).getMechanicId().equals(mechanicId)) {
						textFirstName.setText(mechanicList.get(i).getFirstName());
						textLastName.setText(mechanicList.get(i).getLastName());
						textPhoneNo.setText(mechanicList.get(i).getPhoneNo());
						textEmail.setText(mechanicList.get(i).getEmail());
						textAreaAddress.setText(mechanicList.get(i).getAddress());
						cboMechanicType.setSelectedItem(mechanicList.get(i).getMechanicType());
					}
				}
			}
		});

		displayData(mechanicAction.preapreMechanicList());
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 68, 1244, 381);
		contentPane.add(scrollPane);

		JButton btnAdd = new JButton("New Record");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String mechanicType = String.valueOf(cboMechanicType.getSelectedItem());
				Mechanic mechanic = new Mechanic(textMechanicId.getText(), textFirstName.getText(),
						textLastName.getText(), textPhoneNo.getText(), textEmail.getText(), textAreaAddress.getText(),
						mechanicType);
				List<Mechanic> mechanicList = mechanicAction.addNewMechanic(mechanic);
				JOptionPane.showMessageDialog(null, "New record has inserted successfully.");
				displayData(mechanicList);
				clear();
			}
		});
		btnAdd.setBounds(10, 378, 107, 34);
		panel.add(btnAdd);

		JButton btnNewButton = new JButton("Update Record");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String mechanicType = String.valueOf(cboMechanicType.getSelectedItem());
				Mechanic mechanic = new Mechanic(textMechanicId.getText(), textFirstName.getText(),
						textLastName.getText(), textPhoneNo.getText(), textEmail.getText(), textAreaAddress.getText(),
						mechanicType);
				List<Mechanic> mechanicList = mechanicAction.updateMechanic(mechanic);
				JOptionPane.showMessageDialog(null, "The record has updated successfully.");
				displayData(mechanicList);
				clear();
			}
		});
		btnNewButton.setBounds(134, 378, 130, 34);
		panel.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("Delete Record");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selectedRow = table.getSelectedRow();
				String mechanicId = table.getModel().getValueAt(selectedRow, 0).toString();
				List<Mechanic> mechanicList = mechanicAction.deleteMechanic(mechanicId);
				JOptionPane.showMessageDialog(null, "The record has deleted successfully.");
				displayData(mechanicList);
				clear();
			}
		});
		btnNewButton_1.setBounds(274, 378, 115, 34);
		panel.add(btnNewButton_1);

		lblMechanicType = new JLabel("Mechanic Type");
		lblMechanicType.setBounds(10, 320, 130, 25);
		panel.add(lblMechanicType);

		cboMechanicType = new JComboBox<String>();
		cboMechanicType.addItem("Trainee");
		cboMechanicType.addItem("Workman");
		cboMechanicType.addItem("Master");
		cboMechanicType.setBounds(155, 322, 221, 25);
		panel.add(cboMechanicType);

	}

	private void displayData(List<Mechanic> mechanicList) {
		DefaultTableModel dataModel = new DefaultTableModel() {
			// setting the jtable read only
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		// setting the column name
		Vector<String> header = new Vector<String>();
		header.add("Mechanic ID");
		header.add("First Name");
		header.add("Last Name");
		header.add("Phone No");
		header.add("Email");
		header.add("Address");
		header.add("Mechanic Type");

		dataModel.setColumnIdentifiers(header);
		if (mechanicList == null) {
			this.table.setModel(dataModel);
			return;
		}

		ListIterator<Mechanic> lstrg = mechanicList.listIterator();
		// populating the tablemodel
		while (lstrg.hasNext()) {
			Mechanic mechanic = lstrg.next();
			Vector<String> column = new Vector<String>();
			column.add(mechanic.getMechanicId());
			column.add(mechanic.getFirstName());
			column.add(mechanic.getLastName());
			column.add(mechanic.getPhoneNo());
			column.add(mechanic.getEmail());
			column.add(mechanic.getAddress());
			column.add(mechanic.getMechanicType());
			dataModel.addRow(column);
		}
		// binding the jtable to the model
		this.table.setModel(dataModel);
	}

	private void clear() {
		textFirstName.setText("");
		textLastName.setText("");
		textPhoneNo.setText("");
		textEmail.setText("");
		textMechanicId.setText("");
		textAreaAddress.setText("");
	}
}
