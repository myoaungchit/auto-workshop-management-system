package gui;

import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

public class MenuAction {

	public static JMenuBar initMenu(JFrame parentFrame) {
		JMenuBar menuBar = new JMenuBar();

		JMenu mnHome = new JMenu("Home");
		mnHome.addMenuListener(new MenuListener() {
			public void menuCanceled(MenuEvent e) {
			}

			public void menuDeselected(MenuEvent e) {
			}

			public void menuSelected(MenuEvent e) {
				MainFrame homeScreen = new MainFrame();
				homeScreen.setVisible(true);
				homeScreen.setLocationRelativeTo(null);
				homeScreen.setResizable(true);
				parentFrame.setVisible(false);
			}
		});
		mnHome.setMnemonic(KeyEvent.VK_H);
		menuBar.add(mnHome);

		JMenu mnRepairRequest = new JMenu("Repair Requests");
		mnRepairRequest.addMenuListener(new MenuListener() {

			public void menuSelected(MenuEvent e) {
				RepairRequestUI repairRequestScreen = new RepairRequestUI();
				repairRequestScreen.setVisible(true);
				repairRequestScreen.setLocationRelativeTo(null);
				repairRequestScreen.setResizable(true);
				parentFrame.setVisible(false);

			}

			public void menuDeselected(MenuEvent e) {

			}

			public void menuCanceled(MenuEvent e) {

			}
		});
		mnRepairRequest.setMnemonic(KeyEvent.VK_E);
		menuBar.add(mnRepairRequest);

		JMenu mnJobs = new JMenu("Repair Jobs");
		mnJobs.addMenuListener(new MenuListener() {

			public void menuSelected(MenuEvent e) {
				RepairJobUI repairJobScreen = new RepairJobUI();
				repairJobScreen.setVisible(true);
				repairJobScreen.setLocationRelativeTo(null);
				repairJobScreen.setResizable(true);
				parentFrame.setVisible(false);
			}

			public void menuDeselected(MenuEvent e) {

			}

			public void menuCanceled(MenuEvent e) {

			}
		});
		mnJobs.setMnemonic(KeyEvent.VK_J);
		menuBar.add(mnJobs);
                
                JMenu mnCustomer = new JMenu("Customer");
		mnCustomer.addMenuListener(new MenuListener() {
			public void menuCanceled(MenuEvent e) {
			}

			public void menuDeselected(MenuEvent e) {
			}

			public void menuSelected(MenuEvent e) {
				CustomerUI customerScreen = new CustomerUI();
				customerScreen.setVisible(true);
				customerScreen.setLocationRelativeTo(null);
				customerScreen.setResizable(true);
				parentFrame.setVisible(false);
			}
		});
		mnCustomer.setMnemonic(KeyEvent.VK_C);
		menuBar.add(mnCustomer);
                
                JMenu mnMechanic = new JMenu("Mechanic");
		mnMechanic.addMenuListener(new MenuListener() {

			@Override
			public void menuSelected(MenuEvent e) {
				MechanicUI mechanicScreen = new MechanicUI();
				mechanicScreen.setVisible(true);
				mechanicScreen.setLocationRelativeTo(null);
				mechanicScreen.setResizable(true);
				parentFrame.setVisible(false);
			}

			@Override
			public void menuDeselected(MenuEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void menuCanceled(MenuEvent e) {
				// TODO Auto-generated method stub

			}
		});
		mnMechanic.setMnemonic(KeyEvent.VK_M);
		menuBar.add(mnMechanic);

		JMenu mnStock = new JMenu("Stock");
		mnStock.addMenuListener(new MenuListener() {

			@Override
			public void menuSelected(MenuEvent e) {
				StockUI stockScreen = new StockUI();
				stockScreen.setVisible(true);
				stockScreen.setLocationRelativeTo(null);
				stockScreen.setResizable(true);
				parentFrame.setVisible(false);

			}

			@Override
			public void menuDeselected(MenuEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void menuCanceled(MenuEvent e) {
				// TODO Auto-generated method stub

			}
		});
		mnStock.setMnemonic(KeyEvent.VK_S);
		menuBar.add(mnStock);
                
                

//		JMenu mnReport = new JMenu("Report");
//		mnReport.addMenuListener(new MenuListener() {
//
//			@Override
//			public void menuSelected(MenuEvent e) {
//				// TODO Auto-generated method stub
//
//			}
//
//			@Override
//			public void menuDeselected(MenuEvent e) {
//				// TODO Auto-generated method stub
//
//			}
//
//			@Override
//			public void menuCanceled(MenuEvent e) {
//				// TODO Auto-generated method stub
//
//			}
//		});
//		mnReport.setMnemonic(KeyEvent.VK_R);
//		menuBar.add(mnReport);
		return menuBar;
	}

}
