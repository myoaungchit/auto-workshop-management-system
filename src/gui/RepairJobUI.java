package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import action.CustomerAction;
import action.MechanicAction;
import action.RepairJobAction;
import model.Customer;
import model.Mechanic;
import model.RepairJob;

public class RepairJobUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3731265700236092853L;
	private JPanel contentPane;
	private JTable table;
	private RepairJobAction repairJobAction = new RepairJobAction();
	private CustomerAction custAction = new CustomerAction();
	private JLabel lblRepairRequestList;
	private JTextField textJobId;
	private JTextField textRequestId;
	private MechanicAction mechanicAction = new MechanicAction();
	private JComboBox<ComboItem> comboBox;

	/**
	 * Create the frame.
	 */
	public RepairJobUI() {
		setTitle("Repair Job");
		setJMenuBar(MenuAction.initMenu(this));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setBounds(5, 5, 1280, 1024);
		contentPane = new JPanel();
		contentPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		table = new JTable();
		table.setBounds(0, 0, 1, 1);

		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int selectedRow = table.getSelectedRow();
				String requestId = table.getModel().getValueAt(selectedRow, 0).toString();
				List<RepairJob> repairJobList = repairJobAction.prepareRepairJobList();

				for (int i = 0; i < repairJobList.size(); i++) {
					if (repairJobList.get(i).getRepairRequestId().equals(requestId)) {
						textRequestId.setText(requestId);
					}
				}
			}
		});

		displayData(repairJobAction.prepareRepairJobList());

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 52, 1244, 419);
		contentPane.add(scrollPane);

		lblRepairRequestList = new JLabel("Repair Jobs List");
		lblRepairRequestList.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblRepairRequestList.setBounds(10, 11, 219, 26);
		contentPane.add(lblRepairRequestList);

		JLabel lblNewLabel = new JLabel("Assign Job");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel.setBounds(10, 493, 136, 32);
		contentPane.add(lblNewLabel);

		JPanel panel = new JPanel();
		panel.setBackground(UIManager.getColor("Button.background"));
		panel.setBounds(10, 536, 527, 302);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblNewLabel_1 = new JLabel("Job ID");
		lblNewLabel_1.setBounds(10, 22, 104, 30);
		panel.add(lblNewLabel_1);

		textJobId = new JTextField();
		textJobId.setBounds(171, 27, 313, 30);
		panel.add(textJobId);
		textJobId.setColumns(10);

		JLabel lblNewLabel_2 = new JLabel("Repair Request ID");
		lblNewLabel_2.setBounds(10, 82, 135, 30);
		panel.add(lblNewLabel_2);

		textRequestId = new JTextField();
		textRequestId.setBounds(171, 87, 313, 30);
		panel.add(textRequestId);
		textRequestId.setColumns(10);

		JLabel lblMechanic = new JLabel("Mechanic");
		lblMechanic.setBounds(10, 148, 85, 30);
		panel.add(lblMechanic);

		JButton btnNewButton = new JButton("Submit");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				RepairJob repairJob = new RepairJob();
				repairJob.setRepairJobId(textJobId.getText());
				repairJob.setRepairRequestId(textRequestId.getText());
				Object item = comboBox.getSelectedItem();
				String value = ((ComboItem) item).getKey();
				repairJob.setMechanicId(value);
				List<RepairJob> repairJobList = repairJobAction.addNewRepairJob(repairJob);
				JOptionPane.showMessageDialog(null, "New record has inserted successfully.");
				displayData(repairJobList);
			}
		});
		btnNewButton.setBounds(10, 237, 146, 40);
		panel.add(btnNewButton);

		List<Mechanic> mechanicList = mechanicAction.preapreMechanicList();
		List<ComboItem> comboItemList = new ArrayList<ComboItem>();
		for (int i = 0; i < mechanicList.size(); i++) {
			ComboItem comboItem = new ComboItem(mechanicList.get(i).getMechanicId(),
					mechanicList.get(i).getFirstName() + " " + mechanicList.get(i).getLastName());
			comboItemList.add(comboItem);
		}

		comboBox = new JComboBox<ComboItem>();
		for (int j = 0; j < comboItemList.size(); j++) {
			comboBox.addItem(new ComboItem(comboItemList.get(j).getKey(), comboItemList.get(j).getValue()));

		}
		comboBox.setBounds(171, 153, 313, 40);
		panel.add(comboBox);
	}

	private void displayData(List<RepairJob> repairJobList) {
		DefaultTableModel dataModel = new DefaultTableModel() {
			// setting the jtable read only
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		// setting the column name
		Vector<String> header = new Vector<String>();
		header.add("Repair Request ID");
		header.add("Repair Type");
		header.add("Vehicle Type");
		header.add("Vehicle No");
		header.add("Request Date");
		header.add("Customer");
		header.add("Status");

		dataModel.setColumnIdentifiers(header);
		if (repairJobList == null) {
			this.table.setModel(dataModel);
			return;
		}

		ListIterator<RepairJob> lstrg = repairJobList.listIterator();
		// populating the tablemodel
		while (lstrg.hasNext()) {
			RepairJob repairJob = lstrg.next();
			Vector<String> column = new Vector<String>();
			column.add(repairJob.getRepairRequestId());
			column.add(repairJob.getRepairType());
			column.add(repairJob.getVehicleType());
			column.add(repairJob.getVehicleNo());
			column.add(repairJob.getRepairRequestDate());
			Customer customer = custAction.findCustomerById(repairJob.getCustomerId());
			column.add(customer.getFirstName() + " " + customer.getLastName());
			column.add(repairJob.getStatus());
			dataModel.addRow(column);
		}
		// binding the jtable to the model
		this.table.setModel(dataModel);
	}

	public class ComboItem {
		private String key;
		private String value;

		public ComboItem(String key, String value) {
			this.key = key;
			this.value = value;
		}

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return value;
		}

	}
}
